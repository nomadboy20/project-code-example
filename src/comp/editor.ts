import {reactive, computed} from 'vue'
import {store} from '@/store/index'

const state = reactive({
    show_thanks: false
})

/*
 * Wardrobe params
 */
const width = computed<number>({
    get(): number {
        return store.getters['width']
    },
    async set(value: number) {
        await store.dispatch('SET_WIDTH', value)
    }
})

const height = computed<number>({
    get(): number {
        return store.getters['height']
    },
    async set(value: number) {
        await store.dispatch('SET_HEIGHT', value)
    }
})

const deep = computed<number>({
    get(): number {
        return store.getters['deep']
    },
    async set(value: number) {
        await store.dispatch('SET_DEEP', value)
    }
})

const sectionsLength = computed<number>({
    get(): number {
        return store.getters['sections'].length
    },
    async set(value: number) {
        await store.dispatch('SET_SECTIONS', value)
    }
})

/*
 * General
 */
const isInterior = computed<boolean>((): boolean => store.getters.editMode === 'interior')

/*
 * Wardrobe params validation
 */
const incorrectWidth = computed<boolean>((): boolean => width.value < 40 || width.value > 700)
const incorrectDeep = computed<boolean>((): boolean => deep.value < 20 || deep.value > 100)
const incorrectHeight = computed<boolean>((): boolean => height.value < 40 || height.value > 350)
const minSections = computed<number>(() => Math.floor(width.value / 150))
const maxSections = computed<number>(() => Math.floor(width.value / 30))
const incorrectSections = computed<boolean>(() => sectionsLength.value < minSections.value || sectionsLength.value > maxSections.value)


/*
 * Export
 */

const general = {
    isInterior
}

const wardrobe = {
    width,
    height,
    deep,
    sectionsLength
}

const validation = {
    incorrectWidth,
    incorrectDeep,
    incorrectHeight,
    minSections,
    maxSections,
    incorrectSections
}

export {
    state,
    wardrobe,
    validation,
    general
}