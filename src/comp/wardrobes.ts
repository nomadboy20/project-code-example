const wardrobes = {
    interior: [
        {
            id: 1,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
            ]
        },
        {
            id: 2,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    includesLine: true
                },
                {
                    type: 'shell',
                    size: '4/7',
                    includesLine: true
                },

            ]
        },
        {
            id: 3,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    includesLine: true
                },
                {
                    type: '',
                    size: '4/7',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                    ]
                },
            ]
        },
        {
            id: 4,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    includesLine: true
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '2/7',
                },
            ]
        },
        {
            id: 5,
            parts: [
                {
                    type: '',
                    size: 'full',
                    includesLine: true
                },
            ]
        },
        {
            id: 6,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    childs: [
                        {
                            type: '',
                            size: '1/3',
                        },
                        {
                            type: 'shell',
                            size: '1/3',
                            includesLine: true
                        }
                    ]
                },
                {
                    type: 'shell',
                    size: '4/7',
                    includesLine: true
                },
            ]
        },
        {
            id: 7,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    childs: [
                        {
                            type: '',
                            size: '1/3',
                        },
                        {
                            type: 'shell',
                            size: '1/3',
                            includesLine: true
                        },
                    ]
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '2/7',
                },

            ]
        },
        {
            id: 8,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                    includesDesk: true
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
            ]
        },
        {
            id: 9,
            parts: [
                {
                    type: '',
                    size: '5/7',
                    includesLine: true
                },
                {
                    type: '',
                    size: '2/7',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/2',
                            includesDesk: true
                        },
                        {
                            type: 'desk',
                            size: '1/2',
                        },
                    ]
                },
            ]
        },
        {
            id: 10,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                    includesDesk: true
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
            ]
        },
        {
            id: 11,
            parts: [
                {
                    type: '',
                    size: '4/7',
                    includesLine: true
                },
                {
                    type: '',
                    size: '3/7',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/3',
                            includesDesk: true
                        },
                        {
                            type: 'desk',
                            size: '1/3',
                        },
                        {
                            type: 'desk',
                            size: '1/3',
                        },
                    ]
                },
            ]
        },
        {
            id: 12,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                    includesDesk: true
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
                {
                    type: 'desk',
                    size: '1/7',
                },
            ]
        },
        {
            id: 13,
            parts: [
                {
                    type: '',
                    size: '3/7',
                    includesLine: true
                },
                {
                    type: '',
                    size: '4/7',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                    ]
                },
            ]
        },
        {
            id: 14,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: '',
                    size: '4/7',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true,
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        }
                    ]
                }
            ]
        },
        {
            id: 15,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    includesLine: true
                },
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true,
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        }
                    ]
                }
            ]
        },
        {
            id: 16,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    includesLine: true
                },
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                    ]
                },
            ]
        },
        {
            id: 17,
            parts: [
                {
                    type: '',
                    size: '1/6',
                },
                {
                    type: 'shell',
                    size: '2/6',
                    includesLine: true,
                },
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                        {
                            type: 'desk',
                            size: '1/4',
                        },
                    ]
                },
            ]
        },
        {
            id: 18,
            parts: [
                {
                    type: '',
                    size: '1/6',
                },
                {
                    type: 'shell',
                    size: '2/6',
                    includesLine: true,
                },
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                            includesDesk: true
                        },
                    ]
                },
            ],
        },
        {
            id: 19,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    includesLine: true
                },
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'shell',
                                        size: 'full',
                                        includesLine: true
                                    },
                                ],
                                []
                            ]
                        }
                    ]
                },
            ]
        },
        {
            id: 20,
            parts: [
                {
                    type: 'line',
                    size: '1/2',
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'empty',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        },
        {
            id: 21,
            parts: [
                {
                    type: 'line',
                    size: '1/2',
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'empty',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                        includesDesk: true
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        },
        {
            id: 22,
            parts: [
                {
                    type: 'line',
                    size: '1/2',
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                        // includesDesk: true
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        },
        {
            id: 23,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: '',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                    ]
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'empty',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        },
        {
            id: 24,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: '',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                    ]
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        },
        {
            id: 25,
            parts: [
                {
                    type: '',
                    size: '1/2',
                    childs: [
                        {
                            type: '',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                        {
                            type: 'shell',
                            size: '1/4',
                        },
                    ]
                },
                {
                    type: 'shell',
                    size: '1/2',
                    childs: [
                        {
                            type: 'divider',
                            childs: [
                                [
                                    {
                                        type: 'line',
                                    },
                                ],
                                [
                                    {
                                        type: 'empty',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'shell',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                    {
                                        type: 'desk',
                                        size: '1/4',
                                    },
                                ]
                            ]
                        }
                    ]
                },
            ],
        }
    ],
    exterior: [
        {
            id: 1,
            parts: [
                {
                    type: 'empty',
                    size: 'full',
                },
            ]
        },
        {
            id: 2,
            parts: [
                {
                    type: 'empty',
                    size: '1/2',
                },
                {
                    type: 'shell',
                    size: '1/2',
                },
            ]
        },
        {
            id: 3,
            parts: [
                {
                    type: 'empty',
                    size: '3/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '3/7',
                },
            ]
        },
        {
            id: 4,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '3/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
            ]
        },
        {
            id: 5,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '5/7',
                },
            ]
        },
        {
            id: 6,
            parts: [
                {
                    type: '',
                    size: '5/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
            ]
        },
        {
            id: 7,
            parts: [
                {
                    type: '',
                    size: '2/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '2/7',
                },
                // {
                //     type: '',
                //     size: '1/7',
                // },
            ]
        },
        {
            id: 8,
            parts: [
                {
                    type: '',
                    size: '2/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '4/7',
                },
            ]
        },
        {
            id: 9,
            parts: [
                {
                    type: '',
                    size: '4/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '2/7',
                },
            ]
        },
        {
            id: 10,
            parts: [
                {
                    type: '',
                    size: '1/3',
                },
                {
                    type: '',
                    size: '1/3',
                    childs: [
                        {
                            type: 'shell',
                            size: '3/7',
                        },
                        {
                            type: 'shell',
                            size: '1/7',
                            colorPicker: {
                                top: '-10'
                            }
                        },
                        {
                            type: 'shell',
                            size: '3/7',
                        },
                    ]
                },
                {
                    type: 'shell',
                    size: '1/3',
                },
            ]
        },
        {
            id: 11,
            parts: [
                {
                    type: '',
                    size: '2/7',
                },
                {
                    type: 'shell',
                    size: '3/7',
                },
                {
                    type: 'shell',
                    size: '2/7',
                },
            ]
        },
        {
            id: 12,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '5/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
            ]
        },
        {
            id: 13,
            parts: [
                {
                    type: '',
                    size: '1/7',
                },
                {
                    type: 'shell',
                    size: '5/7',
                },
                {
                    type: 'shell',
                    size: '1/7',
                },
            ]
        },
        {
            id: 14,
            parts: [
                {
                    type: '',
                    size: '1/3',
                },
                {
                    type: 'shell',
                    size: '1/3',
                },
                {
                    type: 'shell',
                    size: '1/3',
                },
            ]
        },
        {
            id: 15,
            parts: [
                {
                    type: '',
                    size: '1/4',
                },
                {
                    type: 'shell',
                    size: '1/4',
                },
                {
                    type: 'shell',
                    size: '1/4',
                },
                {
                    type: 'shell',
                    size: '1/4',
                },
            ]
        },

    ]
}

export default wardrobes