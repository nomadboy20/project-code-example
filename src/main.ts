import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import { store, key } from './store'


/*
 * Import styles
 */
import '@/assets/scss/main.scss'

/*
 * Create app
 */
const app = createApp(App)

/*
 * Install app utils
 */
import utils from './utils'

utils(app)


/*
 * Load app modules
 * - vuex store
 * - vue router
 */

app.use(store, key)
app.use(router)

router.isReady().then(() => {
    app.mount('#app')
})

