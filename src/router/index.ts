import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router'

import Step1 from '@/views/Step1.vue'
import Step2 from '@/views/Step2.vue'
import Step3 from '@/views/Step3.vue'

const routes: Array<RouteRecordRaw> = [

    {
        path: '/',
        name: 'Step1',
        redirect: '/krok-1',
        component: Step1,
    },
    {
        path: '/krok-1',
        name: 'Step1-1',
        component: Step1,
    },
    {
        path: '/krok-2',
        name: 'Step2',
        component: Step2,
    },
    {
        path: '/krok-3',
        name: 'Step3',
        component: Step3,
    },


]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
