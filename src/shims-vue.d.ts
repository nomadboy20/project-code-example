/* eslint-disable */
declare module '*.vue' {
    import type {DefineComponent} from 'vue'
    const component: DefineComponent<{}, {}, any>
    export default component
}

import {Store} from "./store"
import axios from "axios"
import qs from "qs"

declare module "@vue/runtime-core" {
    interface ComponentCustomProperties {
        $store: Store;
        $http: axios;
        $qs: qs;
    }
}