import {InjectionKey} from 'vue'
import {createStore, Store} from 'vuex'

// import _get from '@types/lodash';

import * as _ from "lodash";

export const key: InjectionKey<Store<State>> = Symbol()

export interface State {
    sections: any[],
    deep: number,
    width: number,
    height: number,
    topDesk: boolean,
    editingSection: number,
    wardrobePosition: number,
    editMode: string
}

export interface WardrobeColor {
    color: number,
    column: number,
    path: number[]
}

export const store = createStore<State>({
    state: {

        sections: [
            {
                'interior': {},
                'exterior': {
                    id: 1,
                    parts: [
                        {
                            type: 'empty',
                            size: 'full',
                        },
                    ]
                },
            },
            {
                'interior': {},
                'exterior': {
                    id: 1,
                    parts: [
                        {
                            type: 'empty',
                            size: 'full',
                        },
                    ]
                },
            },
            {
                'interior': {},
                'exterior': {
                    id: 1,
                    parts: [
                        {
                            type: 'empty',
                            size: 'full',
                        },
                    ]
                },
            },
        ],
        deep: 50,
        width: 500,
        height: 250,

        topDesk: true,
        editingSection: 0,
        wardrobePosition: 1,
        editMode: 'interior',

    },

    getters: {

        requiredFieldsFilledUp: (state): boolean => {
            return state.height > 0 && state.width > 0 && state.deep > 0
        },
        getSingleSection: state => (param: any[]) => {
            // return state.sections[param]
        },
        editMode: (state): string => {
            return state.editMode
        },
        editingSection: (state): number => {
            return state.editingSection
        },
        sections: (state): any[] => {
            return state.sections
        },
        topDesk: (state): boolean => {
            return state.topDesk
        },
        height: (state): number => {
            return state.height
        },
        width: (state): number => {
            return state.width
        },
        deep: (state): number => {
            return state.deep
        },
        wardrobePosition: (state): number => {
            return state.wardrobePosition
        }

    },

    actions: {
        SET_SECTION_COLOR({commit}, payload: WardrobeColor) {
            commit('setSectionColor', payload)
        },
        SET_DEEP({commit}, deep: number) {
            commit('setDeep', deep)
        },
        SET_EDIT_MODE({commit}, mode) {
            commit('setEditMode', mode)
        },
        async SET_SECTION_CONTENT({commit, rootGetters}, payload: any) {
            await commit('setSectionContent', {
                'wardrobe_id': payload.id,
                'wardrobe_parts': payload.parts,
                'edit_mode': rootGetters.editMode,
                'editing_section': rootGetters.editingSection
            })
        },
        SET_SECTIONS({commit}, sections: any[]) {
            commit('setSections', sections)
        },
        SET_HEIGHT({commit}, height: number) {
            commit('setHeight', height)
        },
        SET_WIDTH({commit}, width: number) {
            commit('setWidth', width)
        },
        SWITCH_TOP_DESK({commit}, isTopDesk: boolean) {
            commit('switchTopDesk', isTopDesk)
        },
        SET_EDITING_SECTION({commit}, sectionIndex: number) {
            commit('setEditingSection', sectionIndex)
        },
        SET_WARDROBE_POSITION({commit}, val: number) {
            commit('setWardrobePosition', val)
        },
    },

    mutations: {
        setSectionColor(state, payload: WardrobeColor) {
            const finalPath = payload.path.join('.')
            const wardrobe_part = _.get(state.sections[payload.column].exterior.parts, finalPath)
            wardrobe_part['color'] = payload.color
        },
        setEditMode(state, mode: string) {
            state.editMode = mode
        },
        setSectionContent(state, payload) {
            state.sections[payload.editing_section][payload.edit_mode]['wardrobe_id'] = payload.wardrobe_id
            state.sections[payload.editing_section][payload.edit_mode]['parts'] = payload.wardrobe_parts
        },
        setEditingSection(state, sectionIndex: number) {
            state.editingSection = sectionIndex
        },
        switchTopDesk(state, payload) {
            state.topDesk = payload
        },
        setSections(state, payload) {
            state.sections = []
            for (let i = 0; i < payload; i++) {
                state.sections.push({
                    'interior': {},
                    'exterior': {
                        id: 1,
                        parts: [
                            {
                                type: 'empty',
                                size: 'full',
                            },
                        ]
                    },
                })
            }
        },
        setHeight(state, payload: number) {
            state.height = payload
        },
        setWidth(state, payload: number) {
            state.width = payload
        },
        setDeep(state, payload: number) {
            state.deep = payload
        },
        setWardrobePosition(state, payload: number) {
            state.wardrobePosition = payload
        },
    },
})