import {App} from 'vue'
import axios from "axios"
import qs from 'qs'

const install = (app: App) => {
    app.config.globalProperties['$http'] = axios;
    app.config.globalProperties.$qs = qs;
};

export {install as default};
