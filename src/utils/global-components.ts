import {App} from 'vue'
import TestComponent from "@/components/global-components/TestComponent.vue";
import SiteTitle from "@/components/global-components/SiteTitle.vue";


/*
 * Inputs
 */
import BpInput from '@/components/global-components/inputs/BpInput.vue'
import BpSelect from '@/components/global-components/inputs/BpSelect.vue'
import BpSwitch from '@/components/global-components/inputs/BpSwitch.vue'


export default (app: App) => {
    app.component('bp-test-component', TestComponent);
    app.component('bp-site-title', SiteTitle);
    app.component('bp-my-input', BpInput);
    app.component('bp-select', BpSelect);
    app.component('bp-switch', BpSwitch);
}