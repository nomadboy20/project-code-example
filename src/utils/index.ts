import axios from './axios'
import {App} from 'vue'

import globalComponents from './global-components'


export default (app: App) => {

    /*
     * Install axios lib for HTTP requests
     */
    app.use(axios);


    /*
     * Install app global components
     */
    globalComponents(app)

}